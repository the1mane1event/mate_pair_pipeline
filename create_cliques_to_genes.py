import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])



def main():
	df = pd.read_csv(CONSTANTS['FILE_READS_TO_CLIQUES'], sep='\t')
	df2 = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', names=CONSTANTS['FILE_ALIGNMENTS_COLS'])

	df = df.merge(df2, on='read_id')

	df = df[['cliqueid', 'read1_closest_gene']]

	df = df.drop_duplicates()
	uniques = df['cliqueid'].value_counts()
	uniques = uniques[uniques==1]

	df['unique'] = df['cliqueid'].isin(uniques.index)
	df = df.drop_duplicates('cliqueid')

	df['read1_closest_gene'] = np.where(df['unique'], df['read1_closest_gene'], None)

	df = df[['cliqueid', 'read1_closest_gene']]
	df.columns = ['cliqueid', 'geneid']
	df.to_csv(CONSTANTS['FILE_CLIQUES_TO_GENES'], index=False, sep='\t')





if __name__=='__main__':
	main()

