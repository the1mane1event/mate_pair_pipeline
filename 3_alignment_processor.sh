#!/bin/bash
# inputs: 
#    BAM file
# outputs:
#    read alignments file
set -e;

echo "`date +'%m/%d %H:%M:%S'` Starting 3_alignment_processor";
echo "`date +'%m/%d %H:%M:%S'` step 1";
python -u "$BASEDIR"/create_alignment_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 2";
python -u "$BASEDIR"/filter_alignment_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` Done";
