import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
random.seed(1)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])


def main():
	df = pd.read_csv(CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE'], sep='\t')

	df2 = pd.read_csv(CONSTANTS['FILE_CLIQUES_TO_GENES'], sep='\t')

	df = df.merge(df2, on='cliqueid')
	df = df[['geneid', 'cliqueid', 'start_or_end', 'clusterid', 'coordinate', 'count']]
	df.to_csv(CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE'], sep='\t')




if __name__=='__main__':
	main()