import sys,os,time,shutil,io
import pandas as pd
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])


def filter_multiple_alignments(df):
	counts = df['read_id'].value_counts()
	counts = counts[counts==1]

	df = df[df['read_id'].isin(counts.index)]

	return df

def filter_unmapped(df):
	df = df[df['read1_5p']!='None']
	df = df[df['read2_3p']!='None']

	return df

def filter_disagree(df):
	df = df[df['read1_closest_gene']==df['read2_closest_gene']]

	return df
	
def main():
	df = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS'], sep='\t', header=None)
	df.columns = CONSTANTS['FILE_ALIGNMENTS_COLS']

	df = filter_multiple_alignments(df)
	df = filter_unmapped(df)
	df = filter_disagree(df)
	
	df.to_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', header=False, index=False)

if __name__=='__main__':
	main()




