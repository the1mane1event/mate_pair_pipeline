from sklearn import mixture
import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
random.seed(1)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

MIN_BIN_COUNT = 5
VARIANCE = 6.64
N_GAUSSIANS = 20

COUNTER=0
T = time.time()

def filter_lowcount_clusters(clusters, mincount, means_set=None):
	''' Mark clusters without enough data as -1 and renumber the rest '''
	values, counts = np.unique(clusters, return_counts=True)

	col_adj = 0
	for i,c in enumerate(counts):
		if c<MIN_BIN_COUNT:
			clusters[clusters==values[i]] = -1
			col_adj -=1
			if isinstance(means_set, np.ndarray):
				np.delete(means_set, i)
		else:
			clusters[clusters==values[i]] = i + col_adj
	return clusters

def filter1pct(clusters):
	if any(clusters!=-1):
		total = clusters[np.where(clusters!=-1)].shape[0]
		unique, counts = np.unique(clusters, return_counts=True)
		for u,c in zip(unique,counts):
			if u!=-1 and c<.01*total:
				clusters[np.where(clusters==u)] = -1
	return clusters

def cluster_heuristic(X):
	W = 3
	clusters = np.zeros(X.shape)
	cluster2sites = {}
	centers = []
	sites_seen = set()
	vcs = X.value_counts()

	for i in xrange(vcs.shape[0]):
		site_center = int(vcs.index[i])
		if site_center in sites_seen: continue

		cluster2sites[len(cluster2sites)] = []
		centers.append(site_center)
		for site in range(site_center-W, site_center+1+W):
			if site in sites_seen: continue
			sites_seen.add(site)
			cluster2sites[len(cluster2sites)-1].append(site)
		
	for cluster in cluster2sites:
		clusters += X.isin(cluster2sites[cluster]) * cluster

	clusters = filter_lowcount_clusters(clusters, MIN_BIN_COUNT)

	return clusters.astype(np.int32), centers

def choose_gmm(X, n_gaussians):
	lowest_bic = np.infty
	best_n = 0
	bics = []


	for n in xrange(n_gaussians):
		# can not use more Gaussians than data points
		if n+1 > X.shape[0]: continue

		gmm = mixture.GMM(n_components=n+1, n_init=3, covariance_type='tied', params='wm', init_params='wm', n_iter=25)
		
		# fix variance
		gmm.covars_ = np.array([VARIANCE])

		gmm.fit(X)
		bic = gmm.bic(X)
		bics.append(bic)
		if bic < lowest_bic:
			lowest_bic = bic
			best_gmm = gmm
			best_n = n

		#print "Number of components: {} BIC: {}".format(n_gaussians, bic)
	

	return best_gmm

def cluster_gmm(X, n_gaussians):
	gmm = choose_gmm(X, n_gaussians)

	centers = gmm.means_.reshape((-1)).tolist()

	clusters = gmm.predict(X)

	clusters = filter_lowcount_clusters(clusters, MIN_BIN_COUNT, means_set=gmm.means_)

	clusters = filter1pct(clusters)

	return clusters, centers

def apply_cluster_gmm(df_clique, f, f_cluster):
	global COUNTER, T
	COUNTER+=1
	if COUNTER%10==0:
		print COUNTER, "{:.0f} s".format(time.time() - T)
		T = time.time()

	if df_clique.shape[0]<MIN_BIN_COUNT:
		return None

	df_clique['Cluster_Start'], centers_start = cluster_gmm(df_clique['read1_5p'].as_matrix()[:,np.newaxis], N_GAUSSIANS)
	df_clique['Cluster_End'], centers_end = cluster_gmm(df_clique['read2_3p'].as_matrix()[:,np.newaxis], N_GAUSSIANS)

	# if either start or end has only 1 cluster
	if len(df_clique[df_clique['Cluster_Start']!=-1]['Cluster_Start'].unique())<2:
		return None
	if len(df_clique[df_clique['Cluster_End']!=-1]['Cluster_End'].unique())<2:
		return None

	df_clique = df_clique[np.logical_and(df_clique['Cluster_Start']!=-1, df_clique['Cluster_End']!=-1)]
	
	# reads_to_cluster table
	df_clique = df_clique[['read_id', 'cliqueid', 'Cluster_Start', 'Cluster_End']]
	df_clique.to_csv(f, mode='a', index=False, header=False, sep='\t')

	# clique_cluster table
	cliqueid = df_clique['cliqueid'].iloc[0]
	clique_cluster_table = []
	for i,c in enumerate(centers_start):
		count = df_clique[df_clique['Cluster_Start']==i].shape[0]
		if not count: continue
		row = [cliqueid, 'start', i, c, count]
		clique_cluster_table.append(row)
	for i,c in enumerate(centers_end):
		count = df_clique[df_clique['Cluster_End']==i].shape[0]
		if not count: continue
		row = [cliqueid, 'end', i, c, count]
		clique_cluster_table.append(row)

	df = pd.DataFrame(clique_cluster_table)
	df.columns = CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE_COLS']#['cliqueid', 'start_or_end', 'clusterid', 'coordinate', 'count']

	df.to_csv(f_cluster, mode='a', index=False, header=False, sep='\t')

def apply_cluster_heuristic(df_clique, f, f_cluster):
	global COUNTER, T
	COUNTER+=1
	if COUNTER%100==0:
		print COUNTER, "{:.0f} s".format(time.time() - T)
		T = time.time()
	#if COUNTER>1000: sys.exit()

	if df_clique.shape[0]<MIN_BIN_COUNT:
		return None

	
	df_clique['Cluster_Start'], centers_start = cluster_heuristic(df_clique['read1_5p'])
	df_clique['Cluster_End'], centers_end = cluster_heuristic(df_clique['read2_3p'])

	

	# if either start or end has only 1 cluster
	if len(df_clique[df_clique['Cluster_Start']!=-1]['Cluster_Start'].unique())<2:
		return None
	if len(df_clique[df_clique['Cluster_End']!=-1]['Cluster_End'].unique())<2:
		return None

	df_clique = df_clique[np.logical_and(df_clique['Cluster_Start']!=-1, df_clique['Cluster_End']!=-1)]

	# reads_to_cluster table
	df_clique = df_clique[['read_id', 'cliqueid', 'Cluster_Start', 'Cluster_End']]
	df_clique.to_csv(f, mode='a', index=False, header=False, sep='\t')




	# clique_cluster table
	cliqueid = df_clique['cliqueid'].iloc[0]
	clique_cluster_table = []
	for i,c in enumerate(centers_start):
		count = df_clique[df_clique['Cluster_Start']==i].shape[0]
		if not count: continue
		row = [cliqueid, 'start', i, c, count]
		clique_cluster_table.append(row)
	for i,c in enumerate(centers_end):
		count = df_clique[df_clique['Cluster_End']==i].shape[0]
		if not count: continue
		row = [cliqueid, 'end', i, c, count]
		clique_cluster_table.append(row)

	df = pd.DataFrame(clique_cluster_table)
	df.columns = CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE_COLS']#['cliqueid', 'start_or_end', 'clusterid', 'coordinate', 'count']

	df.to_csv(f_cluster, mode='a', index=False, header=False, sep='\t')



if __name__=='__main__':

	# merge read information with cliques
	df_reads_to_cliques = pd.read_csv(CONSTANTS['FILE_READS_TO_CLIQUES'], sep='\t')
	df = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', names=CONSTANTS['FILE_ALIGNMENTS_COLS'])
	df = df_reads_to_cliques.merge(df, on='read_id')
	print df.shape
	print df.iloc[:3,:]
	

	print "Starting clustering..."
	grouped = df.groupby('cliqueid')
	print "Number of cliques: {}".format(len(grouped))
	t = time.time()
	
	print "Creating tmp.txt while clustering..."
	with open('tmp.txt', 'w+') as f:
		with open(CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE'], 'w+') as f_cluster:
			f_cluster.write('{}\n'.format('\t'.join(CONSTANTS['FILE_CLIQUE_CLUSTER_TABLE_COLS'])))
			f.write('{}\n'.format('\t'.join(CONSTANTS['FILE_READS_TO_CLUSTERS_COLS'])))
			if CONSTANTS['CLUSTERING']=='GMM':
				grouped.apply(lambda x: apply_cluster_gmm(x,f,f_cluster) )
			elif CONSTANTS['CLUSTERING']=='MODE':
				grouped.apply(lambda x: apply_cluster_heuristic(x,f,f_cluster) )
			else:
				raise Exception("CLUSTERING in constants.py must be either GMM or MODE, not {}".format(CONSTANTS['CLUSTERING']))

	print "Cliques clustered in {:.0f} s".format(time.time() - t)
	shutil.copy('tmp.txt', CONSTANTS['FILE_READS_TO_CLUSTERS'])
	os.remove('tmp.txt')
	print "Deleted tmp.txt"



	




