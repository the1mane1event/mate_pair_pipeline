from Annotations import Annotations
import pandas as pd
import numpy as np
import sys, os,io 
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def main():
	annotations_gene = Annotations(CONSTANTS['FILE_TRANSCRIPTTABLE'], 'gene_id')
	annotations_transcript = Annotations(CONSTANTS['FILE_TRANSCRIPTTABLE'], 'transcript_id')
	X = 100


	df = annotations_gene.data
	print df.iloc[:3,:]

	zipped = zip(df['gene_id'].tolist(), df['strand'].tolist(), df['chrom'].tolist())

	geneid_to_chromstrand = {a:('p' if b=='1' else 'n',"chr{}".format(c)) for a,b,c in zipped}

	category_a = 0
	category_b = 0
	category_c = 0
	category_d = 0
	category_e = 0

	transcripts_a = set()
	transcripts_b = set()
	transcripts_c = set()
	transcripts_d = set()

	genes_a = set()
	genes_b = set()
	genes_c = set()
	genes_d = set()
	genes_e = set()


	num_lines = file_len(CONSTANTS['FILE_ALIGNMENTS_FILTERED'])
	print num_lines

	with open(CONSTANTS['FILE_ALIGNMENTS_FILTERED']) as f:
		f.next()
		for i,line in enumerate(f):
			if i%(num_lines/20)==0: print "{}% done...".format(5*i/(num_lines/20))
			if i%1000==0: print i
			line = line.strip().split('\t')
			

			if int(line[7]) < int(line[8]):
				start = int(line[7])
				end = int(line[8])
				start_gene_id = line[5]
				end_gene_id = line[6]
				order = ['start', 'end']
			else:
				start = int(line[8])
				end = int(line[7])
				start_gene_id = line[6]
				end_gene_id = line[5]
				order = ['end', 'start']

			strand, ref = geneid_to_chromstrand[start_gene_id]
			nearest_site, r1 = annotations_transcript.nearest_start(start, ref, strand, 'start')
			if np.abs(start-nearest_site) > X:
				r1 = 'novel'

			nearest_site, r2 = annotations_transcript.nearest_start(end, ref, strand, 'end')
			if np.abs(end-nearest_site) > X:
				r2 = 'novel'

			# df_start = df[np.logical_and(df['gene_id']==start_gene_id, df[order[0]]==start)]
			# df_end = df[np.logical_and(df['gene_id']==end_gene_id, df[order[1]]==end)]


			# if df_start.shape[0]==1 and df_end.shape[0]==1:
			# 	r1 = df_start['transcript_id'].tolist()[0]
			# 	r2 = df_end['transcript_id'].tolist()[0]
			# else:
			# 	t = list(set(df_start[order[0]].tolist()).intersection(set(df_end[order[1]].tolist())))
			# 	if t:
			# 		r1 = t[0]
			# 		r2 = t[0]
			# 	else:
			# 		r1 = df_start['transcript_id'].tolist()[0]
			# 		r2 = df_end['transcript_id'].tolist()[0]

			# 	if not ( len(set(df_start[order[0]].tolist()))==1 and len(set(df_end[order[1]].tolist()))==1 ):
			# 		print "bad shapes!"
			# 		print df_start
			# 		print
			# 		print df_end
			# 		sys.exit()

			# if np.abs(int(line[1]) - int(line[7])) > X:
			# 	r1 = 'novel'
			# if np.abs(int(line[2]) - int(line[8])) > X:
			# 	r2 = 'novel'


			if r1=='novel' and r2=='novel':
				category_e+=1
				genes_e.add(start_gene_id)
			elif r2=='novel':
				category_d+=1
				genes_d.add(start_gene_id)
				transcripts_d.add(r1)
			elif r1=='novel':
				category_c+=1
				genes_c.add(end_gene_id)
				transcripts_c.add(r2)
			elif r1==r2:
				category_b+=1
				genes_b.add(start_gene_id)
				transcripts_b.add((r1,r2))
			else:
				category_a+=1
				genes_a.add(start_gene_id)
				transcripts_a.add((r1,r2))

	with open(CONSTANTS['FILE_CONCORDANCE_TABLE'], 'w+') as f:
		total = [category_a, category_b, category_c, category_d, category_e]
		pcts = [1.*num/sum(total) for num in total]
		f.write('Mate pair category\t\% of reads\tUnique transcripts\tUnique genes\n')
		f.write('Mates match single known transcript\t{}\t{}\t{}\n'.format(pcts[0], len(transcripts_a), len(genes_a)))
		f.write('Mates match different known transcript\t{}\t{}\t{}\n'.format(pcts[1], len(transcripts_b), len(genes_b)))
		f.write('Known start, novel end\t{}\t{}\t{}\n'.format(pcts[2], len(transcripts_c), len(genes_c)))
		f.write('Novel start, known end\t{}\t{}\t{}\n'.format(pcts[3], len(transcripts_d), len(genes_d)))
		f.write('Novel start, novel end\t{}\t-\t{}\n'.format(pcts[4], len(genes_e)))



if __name__=="__main__":
	main()


