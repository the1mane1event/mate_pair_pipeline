The mate_pair_pipeline consists of 5 main steps:
  1_reference_compiler.sh
  2_aligner.sh
  3_alignment_processor.sh
  4_concordance_analyzer.sh
  5_dependence_analyzer.sh
Each of these can be run independently, or the entire pipeline can be run be calling master.sh:
  sh master.sh

There are 5 inputs that need to be given at the top of master.sh:
  gtf: the location of the gtf file
  fastas: a comma-separated list without spaces of the fasta files to prepare the reference
  read1s: the location of the fastq file of read1s
  read2s: the location of the fastq file of read2s
  results_dir: the directory where the results of the pipeline should be put (it need not exist prior to calling master.sh)



Additionally, the clique_plotter utility outputs a multi-page pdf of the clique and its row-marginal contingency table. It requires the cliqueid to visualize as an argument (1_1_1 as an example below):
  sh clique_plotter.sh 1_1_1
