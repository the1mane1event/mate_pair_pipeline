import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
random.seed(1)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

T = time.time()

def add_ChromStrand(df_reads, df_annotations):
	df_annotations['read1_closest_gene'] = df_annotations['gene_id']

	df_annotations = df_annotations[['read1_closest_gene','strand','chrom']]
	df_annotations = df_annotations.drop_duplicates()

	df_reads = df_reads.merge(df_annotations, on='read1_closest_gene')

	return df_reads

def merge_endpoints(c1, c2, readids, strand):
	if strand==1:
		s = 's'
		e = 'e'
	else:
		s = 'e'
		e = 's'


	starts = [(coord, s, readids.iloc[i]) for i,coord in enumerate(c1.tolist())]
	ends = [(coord, e, readids.iloc[i]) for i,coord in enumerate(c2.tolist())]

	pts = sorted(starts + ends, key=lambda x: (x[0], -ord(x[1])))
	
	ids = set()
	intervals = []

	for pt in pts:
		if pt[1]=='s':
			ids.add(pt[2])
			open_s = pt[0]
		elif pt[1]=='e':
			if open_s:
				interval = (open_s, pt[0])
				intervals.append((interval, [i for i in ids.copy()]))
			ids.remove(pt[2])
			open_s = 0
		else:
			print "Neither s nor e: {}".format(pt[2])

	return intervals

def get_intervals(annotations_file=None, writefileClique=None, writefileCliqueLevel=None, readsfile=None):
	df = pd.read_csv(readsfile, sep='\t', header=None, names=CONSTANTS['FILE_ALIGNMENTS_COLS'])

	#df = df.iloc[:100,:]
	df_annotations = pd.read_csv(annotations_file, sep='\t', dtype={'chrom':str})

	df = add_ChromStrand(df, df_annotations)

	with open(CONSTANTS['FILE_READS_TO_CLIQUES'], 'w+') as f:
		with open(CONSTANTS['FILE_CLIQUES'], 'w+') as f_cliques:
			f.write('{}\n'.format('\t'.join(CONSTANTS['FILE_READS_TO_CLIQUES_COLS'])))
			f_cliques.write('{}\n'.format('\t'.join(CONSTANTS['FILE_CLIQUES_COLS'])))
			for chrom in CONSTANTS['CHROMS']:
				for strand in [1,-1]:
					chrom = str(chrom.replace('chr', ''))
					chromstrand = "{}_{}".format(chrom, strand)

					df_tmp = df[np.logical_and(df['chrom']==chrom, df['strand']==strand)]
					print chromstrand, df_tmp.shape

					intervals = merge_endpoints(df_tmp['read1_5p'], df_tmp['read2_3p'], df_tmp['read_id'], strand)

					write_clique_table(f, f_cliques, df_tmp, intervals, chromstrand)
					#sys.exit()


	return intervals

def write_clique_table(f, f_cliques, df, intervals, chromstrand):
	for cliqueid,interval in enumerate(intervals):
		cliqueid = '{}_{}'.format(chromstrand, cliqueid)
		df_interval = df[df['read_id'].isin(interval[1])]
		for read in interval[1]:
			s = '{}\n'.format('\t'.join([cliqueid,read]))
			f.write(s)
		
		# write cliques
		s = '{}\t{}\t{}\n'.format(cliqueid, interval[0][0], interval[0][1])
		f_cliques.write(s)


if __name__=='__main__':

	print "Creating intervals and read IDs of cliques..."
	# to create max clique file
	get_intervals(annotations_file=CONSTANTS['FILE_TRANSCRIPTTABLE'], readsfile=CONSTANTS['FILE_ALIGNMENTS_FILTERED'], writefileClique=CONSTANTS['FILE_CLIQUES'], writefileCliqueLevel=CONSTANTS['FILE_CLIQUELEVEL'])
	print "Done!"
