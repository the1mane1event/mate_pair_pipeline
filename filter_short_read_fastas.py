import sys
from itertools import izip
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[5])

def remove_0length_reads(infile1, infile2, outfile1, outfile2):
	with open(infile1) as f1, open(infile2) as f2:
		with open(outfile1, 'a') as g1, open(outfile2, 'a') as g2:
		
			reads_processed = 0
			# store the 4 lines of read together
			row = -1
			read1 = []
			read2 = []

			
			for line1,line2 in izip(f1, f2):
				row+=1
				line1 = line1.strip()
				line2 = line2.strip()
				read1.append(line1)
				read2.append(line2)
				
				# next row will be line 1 of the next read 
				if row==3:
					if len(read1[1])>25 and len(read2[1])>25:
						write_read(read1, g1)
						write_read(read2, g2)

					if not (read1[0].startswith('@') or read1[0].startswith('>')):
						print reads_processed
						print read1
						sys.exit()
					if not (read2[0].startswith('@') or read2[0].startswith('>')):
						print reads_processed
						print read2
						sys.exit()
					if reads_processed%10000==0: print reads_processed


					row = -1
					read1 = []
					read2 = []
					reads_processed+=1

				


def write_read(read, writefile):
	for line in read:
		writefile.write(str(line) + '\n')





if __name__=='__main__':
	r1_files = sys.argv[1].split(',')
	r2_files = sys.argv[2].split(',')

	outfile1 = sys.argv[3]
	outfile2 = sys.argv[4]

	with open(outfile1, 'w+') as g:
		g.write('')
	with open(outfile2, 'w+') as g:
		g.write('')

	for i in xrange(len(r1_files)):
		infile1 = r1_files[i]
		infile2 = r2_files[i]

		remove_0length_reads(infile1, infile2, outfile1, outfile2)


