import pandas as pd
import numpy as np
CHROMS = ['chr{}'.format(num) for num in xrange(1,23)] + ['chrX', 'chrY', 'chrM']

class Annotations(object):
	def __init__(self, fn, column):
		self.fn = fn
		self.parse_file()
		self.chrom_dicts = {}

		for chrom in CHROMS:
			chrom = chrom.replace('chr','')
			for strand in ['p','n']:
				for site in ['start', 'end']:
					col = '{}'.format(site)

					tmp = self.data[self.data['chrom'] == chrom]
					tmp = tmp[tmp['strand'] == ('1' if strand=='p' else '-1')]
					tmp = tmp[[column, col]]
					tmp = tmp.sort_values(col)
					ids = tmp.iloc[:,0]
					sites = tmp.iloc[:,1].as_matrix()

					self.chrom_dicts[chrom + '_' + strand + '_' + site] = (ids,sites)

	def nearest_start(self, value, chrom, strand, site):
		# get rid of 'chr' at beginning
		chrom = chrom[3:]
		# either start/end column name
		col = '{}'.format(site)

		ids, sites = self.chrom_dicts[chrom + '_' + strand + '_' + site]

		# find index of the closest site
		idx = (np.abs(sites-value)).argmin()
		
		# return coordinate and transcript name
		closest_bp = sites[idx]
		transcript = ids.iloc[idx]

		return closest_bp, transcript

	def parse_file(self):
		column_types = {'gene_id':str, 'transcript_id':str, 'start':int, 'end':int, 'strand':str, 'chrom':str}
		self.data = pd.read_csv(self.fn, delimiter='\t', dtype=column_types)