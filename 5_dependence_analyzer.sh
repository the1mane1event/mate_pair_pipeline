#!/bin/bash
# inputs: 
#    filtered alignments file
# outputs:
#    clique table
#    clique cluster table
#    clique counts table
#    clique summary plots

set -e;
echo "`date +'%m/%d %H:%M:%S'` Starting 5_dependence_analyzer..";

echo "`date +'%m/%d %H:%M:%S'` step 1";
python -u "$BASEDIR"/create_reads_to_cliques.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 2";
python -u "$BASEDIR"/create_reads_to_clusters.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 3";
python -u "$BASEDIR"/create_cliques_to_genes.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 4";
python -u "$BASEDIR"/create_clique_counts_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 5";
python -u "$BASEDIR"/create_clique_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 6";
python -u "$BASEDIR"/add_geneid_to_clique_cluster_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 7";
python -u "$BASEDIR"/create_clique_summary_plots.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` Done!";
