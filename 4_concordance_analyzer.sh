#!/bin/bash
# inputs: 
#    filtered alignments file
# outputs:
#    read summary table
#    concordance table
#    read alignment summary plots
set -e;

echo "`date +'%m/%d %H:%M:%S'` Starting 4_concordance_analyzer";
echo "`date +'%m/%d %H:%M:%S'` step 1";
python -u "$BASEDIR"/create_read_summary_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 2";
python -u "$BASEDIR"/create_concordance_table.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` step 3";
python -u "$BASEDIR"/create_read_alignment_summary_plots.py $results_dir;

echo "`date +'%m/%d %H:%M:%S'` Done!";