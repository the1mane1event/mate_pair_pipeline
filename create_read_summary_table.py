# -*- coding: utf-8 -*-
import sys, os, io
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])


def main():
	unmapped = 0
	mapped = 0
	ids = set()
	agree = 0
	with open(CONSTANTS['FILE_ALIGNMENTS']) as f:
		f.next()
		for line in f:
			line = line.strip().split('\t')

			# mate positions are in 1,2 index of tsv
			for mate in [1,2]:
				if line[mate]=='None':
					unmapped+=1
				else:
					mapped+=1

			ids.add(line[0])

			if line[5]==line[6]:
				agree+=1



	with open(CONSTANTS['FILE_READ_SUMMARY_TABLE'], 'w+') as f:
		f.write('Cleaned reads\t{}\n'.format(unmapped+mapped))
		f.write('Mapped reads\t{}\n'.format(mapped))
		f.write('Reads w/o multiple alignments\t{}\n'.format(len(ids)*2))
		f.write('Reads whose mate’s closest annotated transcript is an isoform of the same gene\t{}\n'.format(agree))





if __name__=='__main__':
	main()

