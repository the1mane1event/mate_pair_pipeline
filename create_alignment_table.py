import pysam, time, sys, io
import pandas as pd
import numpy as np
from Annotations import Annotations
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

class Reader(object):
	def __init__(self, bam_file, outfile, limit):
		self.bam_file = bam_file
		self.outfile = outfile
		self.limit = limit
		self.reads = {}
		self.unmapped_reads = 0
		self.mapped_reads = 0
		self.matches = 0
		self.non_matches = 0

	def parse_reads(self, annotations):
		self.annotations = annotations
		samfile = pysam.AlignmentFile(self.bam_file, "rb")

		mates = {}
		i=0
		skipped = set()
		#refs = set()
		with open(self.outfile, 'w+') as g:
			g.write("{}\n".format("\t".join(CONSTANTS['FILE_ALIGNMENTS_COLS'])))
			print "Fetching alignments for reads..."
			for read in samfile.fetch(until_eof=True):
				sys.stdout.flush()
				if read.is_unmapped:
					self.unmapped_reads+=1
					continue
				else:
					self.mapped_reads+=1

				if read.reference_name not in CONSTANTS['CHROMS']:
					skipped.add(read.reference_name)
					continue

				i+=1

				if i>self.limit:
					print "Breaking"
					break

				if i%100000==0: print i

				hit_id = read.get_tag('HI')
				if not read.query_name in mates:
					mates[read.query_name] = {}

				if not hit_id in mates[read.query_name]:
					mates[read.query_name][hit_id] = [None,None]

				if read.is_read1:
					mates[read.query_name][hit_id][0] = read
				else:
					mates[read.query_name][hit_id][1] = read

				if all(mates[read.query_name][hit_id]):
					r1, r2 = mates[read.query_name][hit_id]
					self.print_row(g, r1, r2)
					del mates[read.query_name][hit_id]

			print skipped
			print self.mapped_reads
			print self.unmapped_reads
			print "Fetched alignments for {} reads".format(len(mates))
			print "Printing alignment pairs for reads..."

			for i,k in enumerate(mates):
				if i%10000==0: print i


				for hit_id in mates[k]:
					r1 = mates[k][hit_id][0]
					r2 = mates[k][hit_id][1]
					if r1 and r2:
						print "Something's wrong..."
						sys.exit()

					self.print_row(g, r1, r2)

			print "Done!"	

	def print_row(self, fn, r1, r2):
		
		if r1:
			readname = r1.query_name
			ref = r1.reference_name
		else:
			readname = r2.query_name
			ref = r2.reference_name

		r1_start = r1_len = closest_start = gene_start = r2_end = r2_len = closest_end = gene_end = None

		if (r2 and r2.is_reverse) or (r1 and not r1.is_reverse):
			strand = 'p'

			if r1 and not r1.is_unmapped:
				r1_start = r1.reference_start
				r1_len = r1.query_alignment_length
				closest_start, gene_start = self.annotations.nearest_start(r1_start, ref, strand, 'start')

			if r2 and not r2.is_unmapped:
				r2_end = r2.reference_end - 1 #reference_end points to 1 past the last aligned pos
				r2_len = r2.query_alignment_length
				closest_end, gene_end = self.annotations.nearest_start(r2_end, ref, strand, 'end')


		else:
			strand = 'n'

			if r1 and not r1.is_unmapped:
				r1_start = r1.reference_end - 1 #the most 5' end of a reverse strand
				r1_len = r1.query_alignment_length
				closest_start, gene_start = self.annotations.nearest_start(r1_start, ref, strand, 'start')

			if r2 and not r2.is_unmapped:
				r2_end = r2.reference_start
				r2_len = r2.query_alignment_length
				closest_end, gene_end = self.annotations.nearest_start(r2_end, ref, strand, 'end')

		if gene_start and gene_end and gene_start==gene_end:
			self.matches+=1
		else:
			self.non_matches+=1

		fn.write("\t".join([str(x) for x in [
				readname,
				r1_start,
				r2_end,
				r1_len,
				r2_len,
				gene_start,
				gene_end,
				closest_start,
				closest_end]]) + '\n')


def main():
	t = time.time()
	print "Parsing annotations..."
	annotations = Annotations(CONSTANTS['FILE_TRANSCRIPTTABLE'], 'gene_id')
	print "Parsed annotations in {:.2f} s\n".format(time.time() - t)

	reader = Reader(CONSTANTS['FILE_BAM'], CONSTANTS['FILE_ALIGNMENTS'], limit=sys.maxint)#limit=250000)#
	t = time.time()
	print "Parsing reads..."
	reader.parse_reads(annotations)
	print "Parsed reads in {:.2f} s\n".format(time.time() - t)
	

	print "{}/{} reads mapped".format(reader.mapped_reads, reader.unmapped_reads+reader.mapped_reads)
	#print "{} reads had multiple alignments".format(reader.multiple_alignments)
	print "{}/{} read pairs aligned to the same gene".format(reader.matches, reader.matches+reader.non_matches)



















if __name__=="__main__":
	main()




