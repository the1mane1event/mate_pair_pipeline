#!/bin/bash
# inputs: 
#    clean reads
#    aligner reference
# outputs:
#    BAM file




set -e;
echo "`date +'%m/%d %H:%M:%S'` Starting 2_aligner";
python "$BASEDIR"/filter_short_read_fastas.py $read1s $read2s $cleaned_read1s $cleaned_read2s $results_dir;

# rsem-calculate-expression 
#     -p 8 
#     --forward-prob=.5 
#     --paired-end 
#     --star 
#     --time 
#     --star-output-genome-bam 
#     $2 
#     $3 
#     "$1"/ref/ref 
#     "$1"/rsem_alignment/alignment;

export starcommand='
STAR 
 --genomeDir '$1'/ref 
 --outSAMunmapped Within 
 --outFilterType BySJout 
 --outSAMattributes NH HI AS NM MD 
 --outFilterMultimapNmax 20 
 --outFilterMismatchNmax 999 
 --outFilterMismatchNoverLmax 0.04 
 --alignIntronMin 20 
 --alignIntronMax 1000000 
 --alignMatesGapMax 1000000 
 --alignSJoverhangMin 8 
 --alignSJDBoverhangMin 1 
 --sjdbScore 1 
 --runThreadN 8 
 --genomeLoad NoSharedMemory 
 --outSAMtype BAM Unsorted 
 --quantMode TranscriptomeSAM 
 --outSAMheaderHD @HD VN:1.4 SO:unsorted 
 --outFileNamePrefix '$1'/rsem_alignment/alignment 
 --readFilesIn '$2' '$3'
'
echo $starcommand;
$starcommand;
 
samtools sort "$1"/rsem_alignment/alignment.STAR.genome.bam -o "$1"/rsem_alignment/sorted.STAR.genome.bam

samtools index "$1"/rsem_alignment/sorted.STAR.genome.bam "$1"/rsem_alignment/sorted.STAR.genome.bam.bai
echo "`date +'%m/%d %H:%M:%S'` Done";
