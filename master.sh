#!/bin/bash
set -e;
export gtf="/tier2/deweylab/scratch/mamodio/gtf/chroms_and_spikes.gtf";
export fastas="/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr21.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr19.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr18.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr20.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr22.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr8.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr9.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chrY.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chrX.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr3.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr14.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr15.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr2.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr17.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr16.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr1.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr12.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr5.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr4.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr13.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr11.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr6.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr7.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chr10.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/chroms/chrM.fa,/tier2/deweylab/scratch/mamodio/fasta_chroms_and_spikes/SIRV_150601a.fasta";


export read1s='/tier2/deweylab/scratch/cdewey/lucigen/20161003-cDNA-MatePairs.start.fastq'
export read2s='/tier2/deweylab/scratch/cdewey/lucigen/20161003-cDNA-MatePairs.end.fastq'

export results_dir="/tier2/deweylab/scratch/mamodio/mate_pair_pipeline/results";

export BASEDIR=$(dirname "$0");

# make the results directory if it doesn't already exist
if [ ! -d "$results_dir" ] ; then 
	mkdir "$results_dir";
fi

# this is where the FASTQs filtered for read length of at least 25bp will go
if [ ! -d "$results_dir"/cleaned_fastqs ] ; then 
	mkdir "$results_dir"/cleaned_fastqs;
fi
export cleaned_read1s="$results_dir"/cleaned_fastqs/start.fastq;
export cleaned_read2s="$results_dir"/cleaned_fastqs/end.fastq;

# where the RSEM reference files will go
if [ ! -d "$results_dir"/ref ] ; then 
	mkdir "$results_dir"/ref;
fi
# where the alignment results will go
if [ ! -d "$results_dir"/rsem_alignment ] ; then 
	mkdir "$results_dir"/rsem_alignment;
fi
# where the alignment plots will go
if [ ! -d "$results_dir"/read_alignment_summary_plots ] ; then 
	mkdir "$results_dir"/read_alignment_summary_plots;
fi
# where the clique plots will go
if [ ! -d "$results_dir"/clique_plots ] ; then 
	mkdir "$results_dir"/clique_plots;
fi

python "$BASEDIR"/create_constants.py $gtf $results_dir;

sh "$BASEDIR"/1_reference_compiler.sh $results_dir $gtf $fastas;

sh "$BASEDIR"/2_aligner.sh $results_dir $cleaned_read1s $cleaned_read2s;

sh "$BASEDIR"/3_alignment_processor.sh;

sh "$BASEDIR"/4_concordance_analyzer.sh;

sh "$BASEDIR"/5_dependence_analyzer.sh;

date +'%m/%d %H:%M:%S'; echo "Done with all steps!";

