import sys
import cPickle

def load_variables(results_dir):
	with open('{0}/constants.pkl'.format(results_dir), 'rb') as f:
		d = cPickle.load(f)
	return d

if __name__=='__main__':
	gtf = sys.argv[1]
	results_dir = sys.argv[2]
	d = {}

	d['CLUSTERING'] =  'GMM'#'MODE'#

	d['RESULTS_DIR'] = '{0}'.format(results_dir)
	d['FILE_GTF'] = '{0}'.format(gtf)
	d['FILE_BAM'] = '{0}/rsem_alignment/sorted.STAR.genome.bam'.format(results_dir)
	d['FILE_EXONS'] = '{0}/exons.csv'.format(results_dir)
	d['FILE_TRANSCRIPTTABLE'] = '{0}/transcript_table.tsv'.format(results_dir)
	d['FILE_ALIGNMENTS'] = '{0}/read_alignments.txt'.format(results_dir)
	d['FILE_ALIGNMENTS_FILTERED'] = '{0}/read_alignments_filtered.txt'.format(results_dir)
	d['FILE_READS_TO_CLIQUES'] = '{0}/reads_to_cliques.tsv'.format(results_dir)
	d['FILE_READS_TO_CLUSTERS'] = '{0}/reads_to_clusters.tsv'.format(results_dir)
	d['FILE_CLIQUE_COUNTS_TABLE'] = '{0}/clique_counts_table.tsv'.format(results_dir)
	d['FILE_CLIQUE_CLUSTER_TABLE'] = '{0}/clique_cluster_table.tsv'.format(results_dir)
	d['FILE_CLIQUE_TABLE'] = '{0}/clique_table.tsv'.format(results_dir)
	d['FILE_CLIQUES'] = '{0}/cliques.tsv'.format(results_dir)
	d['FILE_CLIQUELEVEL'] = '{0}/cliquelevel.csv'.format(results_dir)
	d['FILE_CLIQUES_TO_GENES'] = '{0}/cliques_to_genes.tsv'.format(results_dir)
	d['FILE_READ_SUMMARY_TABLE'] = '{0}/read_summary_table.tsv'.format(results_dir)
	d['FILE_CONCORDANCE_TABLE'] = '{0}/concordance_table.tsv'.format(results_dir)
	d['DIR_CLIQUE_PLOTS'] = '{0}/clique_plots'.format(results_dir)
	d['DIR_READ_ALIGNMENT_SUMMARY_PLOTS'] = '{0}/read_alignment_summary_plots'.format(results_dir)
	d['FILE_ALIGNMENTS_COLS'] = ['read_id', 'read1_5p', 'read2_3p', 'read1_len', 'read2_len', 'read1_closest_gene', 'read2_closest_gene', 'read1_closest_site', 'read2_closest_site']
	d['FILE_READS_TO_CLIQUES_COLS'] = ['cliqueid', 'read_id']
	d['FILE_READS_TO_CLUSTERS_COLS'] = ['read_id', 'cliqueid', 'Cluster_Start', 'Cluster_End']
	d['FILE_CLIQUE_COUNTS_TABLE_COLS'] = ['gene_id', 'cliqueid', 'Cluster_Start', 'Cluster_End']
	d['FILE_CLIQUE_CLUSTER_TABLE_COLS'] = ['cliqueid', 'start_or_end', 'clusterid', 'coordinate', 'count']
	d['FILE_CLIQUES_COLS'] = ['cliqueid', 'clique_start', 'clique_end']
	d['FILE_CLIQUELEVEL_COLS'] = ['Clique', 'Clique_Start', 'Clique_End', 'p-value', 'MI', 'ABS_MI', 'L1_NORM', 'Expression_Level', 'bh']
	d['FILE_CLIQUES_TO_GENES_COLS'] = ['Clique', 'Gene']
	d['group_col'] = 'Clique'
	d['start_col'] = 'Clique_Start'
	d['end_col']	= 'Clique_End'
	d['START_CLUSTER_COL'] = 'Cluster_Start'
	d['END_CLUSTER_COL'] = 'Cluster_End'
	d['CHROMS'] = ['chr{}'.format(num) for num in xrange(1,23)] + ['chrX', 'chrY', 'chrM']
	
	with open('{0}/constants.pkl'.format(results_dir), 'wb+') as f:
		cPickle.dump(d, f)

