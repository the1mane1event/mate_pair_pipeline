import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import NullFormatter, MaxNLocator
import numpy as np

def row_marginal_contingency_plot(counts, fig, savefile):
	x = counts.sum(axis=0)
	y = counts.sum(axis=1)

	row_marginals = 1.*counts / y[:,np.newaxis]

	# Set up your x and y labels
	xlabel = 'End Site Cluster'
	ylabel = 'Start Site Cluster'
	 
	# Define the locations for the axes
	left, width = 0.12, 0.55
	bottom, height = 0.12, 0.55
	bottom_h = left_h = left+width+0.02
	 
	# Set up the geometry of the three plots
	rect_temperature = [left, bottom, width, height] # dimensions of temp plot
	rect_histx = [left, bottom_h, width, 0.25] # dimensions of x-histogram
	rect_histy = [left_h, bottom, 0.25, height] # dimensions of y-histogram
	 
	# Make the three plots
	axTemperature = fig.add_axes(rect_temperature) # temperature plot
	axHistx = fig.add_axes(rect_histx) # x histogram
	axHisty = fig.add_axes(rect_histy) # y histogram

	# Remove the inner axes numbers of the histograms
	nullfmt = NullFormatter()
	axHistx.xaxis.set_major_formatter(nullfmt)
	axHisty.yaxis.set_major_formatter(nullfmt)



	axTemperature.imshow(row_marginals, interpolation='nearest', aspect='auto', cmap='coolwarm')
	for (j,i),label in np.ndenumerate(row_marginals):
		val = "{:0.2f}".format(label)
		if val.startswith('1'):
			val = '1.0'
		else:
			val = val[1:]
		#val = counts[j,i]
		if counts.shape[1]>20:
			fs = 'xx-small'
		elif counts.shape[1]>10:
			fs = 'small'
		else:
			fs = 'medium'
		axTemperature.text(i, j, val, ha='center', va='center', fontsize=fs)



	#Plot the axes labels
	axTemperature.set_xlabel(xlabel,fontsize=20)
	axTemperature.set_ylabel(ylabel,fontsize=20)

	# Remove axis ticks
	axTemperature.set_xticks([])
	axTemperature.set_yticks([])

	#Plot the histograms
	color_listx = [cm.coolwarm(1.*num/max(x)) for num in x]
	color_listy = [cm.coolwarm(1.*num/max(y)) for num in np.flipud(y)]

	axHistx.bar(np.arange(counts.shape[1]), x, 1, hatch='|', color=color_listx)

	axHisty.barh(np.arange(counts.shape[0]), np.flipud(y), 1, hatch='-', color=color_listy)

	axHistx.set_xlim([0,counts.shape[1]])
	axHisty.set_ylim([0,counts.shape[0]])

	#Make the tickmarks pretty
	ticklabels = axHistx.get_yticklabels()
	for label in ticklabels:
		label.set_fontsize(8)
		label.set_family('serif')
		
	#Make the tickmarks pretty
	ticklabels = axHisty.get_xticklabels()
	for label in ticklabels:
			label.set_fontsize(8)
			label.set_family('serif')

	axHisty.xaxis.set_major_locator(MaxNLocator(3))
	axHistx.yaxis.set_major_locator(MaxNLocator(3))


	#plt.draw()
	#plt.savefig("{}.pdf".format(savefile), format='pdf', transparent=True)
