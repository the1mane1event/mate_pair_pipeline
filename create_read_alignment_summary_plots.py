import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys, io
from matplotlib.backends.backend_pdf import PdfPages
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def annotation_recovery(pdf, df_reads):
	df_an = pd.read_csv(CONSTANTS['FILE_TRANSCRIPTTABLE'], sep='\t', dtype={'chrom':str})

	df_an['strand'] = df_an['strand'].map(lambda x: 'p' if x==1 else 'n')
	df_an['chrom'] = 'chr' + df_an['chrom'].map(str) + '_' + df_an['strand']
	df_an = df_an[['chrom','start','end','gene_id']].drop_duplicates()

	df_an['key'] = 0
	df_reads['key'] = 0

	all_results = []
	for cols in [['read1_5p', 'read1_closest_site', 'Start'], ['read2_3p', 'read2_closest_site', 'End']]:

		df_reads['diff'] = (df_reads[cols[0]] - df_reads[cols[1]]).abs()
		
		grouped = df_reads.groupby(cols[0])

		applied = grouped.apply(lambda x: x['diff'].min())

		applied = applied.sort_values(ascending=True)
		applied = applied.reset_index()
		applied.columns = [applied.columns[0], 'diff']
		all_results.append(applied)

		# plot of pct annotated sites recovered
		fig,ax = plt.subplots(1,1)
		df = pd.concat(all_results)

		df = df.sort_values('diff',ascending=True)
		vals = df['diff'].unique()
		for v in np.linspace(np.log2(1), np.log2(vals[-1]), 100):
			count = df[df['diff']<2**v].shape[0]
			if not v: continue
			ax.scatter(v,1.*count/df.shape[0])

		ax.set_xlabel('Log2(x)')
		ax.set_ylabel('Pct of Annotated {} Sites Recovered'.format(cols[2]))
		ax.set_title('Annotated {} sites with a read within x'.format(cols[2]))
		ax.set_ylim([0,1])
		pdf.savefig(fig)
		#fig.savefig('{}/recovered_{}_sites'.format(DIR_READ_ALIGNMENT_SUMMARY_PLOTS, cols[2]))

def closest_site_diff(pdf, df_reads):
	for read_num in ['1_5p','2_3p']:
		diffs = df_reads['read{}_closest_site'.format(read_num[0])] - df_reads['read{}'.format(read_num)]
		diffs = diffs.abs().value_counts().reset_index().sort_values(by='index')
		total_bp_away = 0
		count = 0
		plot_x = []
		plot_y = []
		for i in xrange(diffs.shape[0]):
			if diffs['index'].iloc[i]==0: continue
			total_bp_away = np.log2(diffs['index'].iloc[i])
			count +=diffs[0].iloc[i]
			plot_x.append(total_bp_away)
			plot_y.append(1.*count/df_reads.shape[0])

		fig,ax = plt.subplots(1,1)
		ax.scatter(plot_x, plot_y)
		ax.set_xlabel('Log2(x)')
		ax.set_ylabel('Pct of reads')
		ax.set_title('Reads within x bp from Closest Annotated Read{} Site'.format(read_num))
		ax.set_ylim([0,1])
		pdf.savefig(fig)
		#fig.savefig('{}/reads_within_x_read{}_site_nonlog'.format(DIR_READ_ALIGNMENT_SUMMARY_PLOTS, read_num))

def recovered_sites_per_gene(pdf, df_reads):
	df_an = pd.read_csv(CONSTANTS['FILE_TRANSCRIPTTABLE'], sep='\t', dtype={'chrom':str})

	df_an['strand'] = df_an['strand'].map(lambda x: 'p' if x==1 else 'n')
	df_an['chrom'] = 'chr' + df_an['chrom'].map(str) + '_' + df_an['strand']
	df_an = df_an[['chrom','start','end','gene_id']].drop_duplicates()

	df_an['key'] = 0
	df_reads['key'] = 0

	for cols in [['read1_5p', 'read1_closest_site', 'read1_closest_gene', 'Start'], ['read2_3p', 'read2_closest_site', 'read2_closest_gene', 'End']]:

		all_results = {}
		c1 = cols[1]
		c2 = cols[2]
		c3 = cols[0]


		grouped = df_reads.groupby(c2)
		applied = grouped.apply(lambda x: len(x[c1].unique()))
		vc = applied.value_counts()

		for i in xrange(len(vc)):
			v = vc.index[i]
			c = vc.iloc[i]
			if v not in all_results:
				all_results[v] = 0
			all_results[v]+=c


		fig,ax = plt.subplots(1,1)
		x = all_results.keys()
		y = [all_results[k] for k in all_results.keys()]
		ax.scatter(x,y)
		ax.set_xlabel('Sites per gene')
		ax.set_ylabel('Number of genes')
		ax.set_title('Number of recovered {} sites per gene'.format(cols[3]))
		ax.set_ylim([0,max(y)])
		pdf.savefig(fig)
		#fig.savefig('{}/sites_per_gene_{}'.format(DIR_READ_ALIGNMENT_SUMMARY_PLOTS, cols[3]))

def genomic_distance_by_mates_agree_disagree(pdf):
	with open(CONSTANTS['FILE_ALIGNMENTS']) as f:
		f.next()
		agree = []
		disagree = []
		for line in f:
			line = line.strip().split('\t')

			if line[1]=='None' or line[2]=='None': continue

			r1 = int(line[1])
			r2 = int(line[2])

			if np.abs(r1-r2)==0: continue

			if line[5]==line[6]:
				agree.append(np.log2(np.abs(r1-r2)))
			else:
				disagree.append(np.log2(np.abs(r1-r2)))

	data = [np.array(agree).reshape((-1,1)), np.array(disagree).reshape((-1,1))]
	pos = [1, 2]
	label = ['Mates agree', 'Mates disagree']
	fig,ax = plt.subplots(1,1)
	ax.violinplot(data, pos, showmeans=True, showextrema=True, showmedians=False)
	ax.set_xticks(pos)
	ax.set_xticklabels(label)
	ax.set_ylabel('Log2(genomic distance)')
	pdf.savefig(fig)
	#fig.savefig('{}/distances_startend'.format(DIR_READ_ALIGNMENT_SUMMARY_PLOTS))

def main():
	df_reads = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', header=None, names=CONSTANTS['FILE_ALIGNMENTS_COLS'])
	with PdfPages('{}/all_plots.pdf'.format(CONSTANTS['DIR_READ_ALIGNMENT_SUMMARY_PLOTS'])) as pdf:
		print "Plotting annotation_recovery..."
		annotation_recovery(pdf, df_reads)
		print "Done!"
		print "Plotting closest site difference..."
		closest_site_diff(pdf, df_reads)
		print "Done!"
		print "Plotting recovered sites per gene..."
		recovered_sites_per_gene(pdf, df_reads)
		print "Done!"
		print "Plotting genomic distances by whether mates agree or disagree..."
		genomic_distance_by_mates_agree_disagree(pdf)
		print "Done!"






if __name__=='__main__':
	main()



