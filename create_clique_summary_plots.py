import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore", category=DeprecationWarning)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def benjamini_hochberg(p_values, alpha=.05):
	p_values_sorted = sorted(p_values)

	#largest_k = 0
	reject = {}
	for i,p in enumerate(p_values_sorted):
		# one-indexed rank, not zero-indexed
		i = i+1
		
		thresh = i * alpha / len(p_values_sorted)
		if p < thresh:
			#largest_k = i
			reject[p] = 1
		else:
			reject[p] = 0

	reject_unsorted = []
	for p in p_values:
		reject_unsorted.append( reject[p] )

	return reject_unsorted

def plot_mi_histogram(df):
	df = df[df['p-value_adjusted']<.05]
	fig, ax = plt.subplots(1,1)

	ax.hist(df['MI'].tolist())
	ax.set_xlabel('MI')
	ax.set_ylabel('Count')
	ax.set_title('Significant Cliques with FDR .05')
	fig.savefig('{}/significant_cliques_by_mi'.format(CONSTANTS['DIR_READ_ALIGNMENT_SUMMARY_PLOTS']))



def main():
	df = pd.read_csv(CONSTANTS['FILE_CLIQUE_TABLE'], sep='\t')
	plot_mi_histogram(df)

if __name__=='__main__':
	main()