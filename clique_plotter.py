import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os
import matplotlib.pyplot as plt
import matplotlib.colors as colors
cmap = plt.get_cmap('rainbow')
from rmct import row_marginal_contingency_plot
from matplotlib.backends.backend_pdf import PdfPages
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def reorder_clusters(counts, start_order, end_order):
	counts = counts.reindex(start_order)

	counts = counts[end_order]
	
	return counts.as_matrix()

def gene_rmct(pdf, df, mi, clique):
	print "Plotting rmct for {}.".format(clique)
	chrom,strand,clique_id = clique.split('_')

	start_order = df.sort_values('read1_5p').drop_duplicates('Cluster_Start')['Cluster_Start'].tolist()
	end_order = df.sort_values('read2_3p').drop_duplicates('Cluster_End')['Cluster_End'].tolist()

	counts = pd.crosstab(df['Cluster_Start'], df['Cluster_End'])

	if counts.shape[0]<2 or counts.shape[1]<2:
		return

	counts = reorder_clusters(counts, start_order, end_order)

	fig = plt.figure()
	row_marginal_contingency_plot(counts, fig, '{}/rmct_{}_{:.3f}'.format(CONSTANTS['DIR_CLIQUE_PLOTS'], clique, mi))
	pdf.savefig(fig)
	plt.close('all')

def plot_clique(pdf, df, mi, clique, clique_start, clique_end):
	print "Plotting clique plot for {}.".format(clique)
	chrom,strand,clique_id = clique.split('_')
	df = df.sort_values('read1_5p')
	
	fig, ax = plt.subplots(1,1)
	
	starts = df['read1_5p'].tolist()
	ends = df['read2_3p'].tolist()
	startclusters = df['Cluster_Start'].tolist()
	endclusters = df['Cluster_End'].tolist()
	l = df.shape[0]

	# plot each read on y-axis integer like:     o- - - -o
	num_start_clusters = len(set(startclusters))
	num_end_clusters = len(set(endclusters))
	norm_start = colors.Normalize(vmin=0,vmax=num_start_clusters)
	norm_end = colors.Normalize(vmin=0,vmax=num_end_clusters)
	for i,(s,e,sc,ec) in enumerate(zip(starts,ends,startclusters,endclusters)):
		ax.scatter(s, i, marker='o', c=cmap(norm_start(sc)))
		ax.scatter(e, i, marker='o', c=cmap(norm_end(ec)))
		ax.plot([s,e], [i,i], linewidth=1, linestyle='--', marker='None', c=cmap(norm_start(sc)))

	# plot clique interval as a thick black box
	i0 = clique_start
	i1 = clique_end

	ax.plot([i0,i1], [-.5,-.5], c='k', linewidth=1.5)
	ax.plot([i0,i0], [-.5,l-.5], c='k', linewidth=1.5)
	ax.plot([i1,i1], [-.5,l-.5], c='k', linewidth=1.5)
	ax.plot([i0,i1], [l-.5,l-.5], c='k', linewidth=1.5)

	ax.set_title('{} strand {} clique {}: MI {:.3f}'.format(chrom,strand,clique_id,mi))
	ax.set_xlabel('Position on chr{}'.format(chrom))
	ax.set_ylabel('Read IDs')

	plot_exons(ax, chrom, min(starts), max(ends))

	pdf.savefig(fig)

def plot_exons(ax, chrom, start, end):
	thischrom = EXONS[EXONS['chrom'].map(lambda x: x.replace('chr',''))==chrom]
	all_exons_to_plot = set()

	# situation 1: exon includes start position but not the end position
	exons_to_plot = thischrom[thischrom['exon_start']<start]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_end']>=start]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_end']<=end]
	zipped = zip(exons_to_plot['exon_start'].tolist(),exons_to_plot['exon_end'].tolist())
	[all_exons_to_plot.add(z) for z in zipped]

	# situation 2: exon is entirely contained within clique
	exons_to_plot = thischrom[thischrom['exon_start']>=start]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_end']<=end]
	zipped = zip(exons_to_plot['exon_start'].tolist(),exons_to_plot['exon_end'].tolist())
	[all_exons_to_plot.add(z) for z in zipped]

	# situation 3: exon includes end position but not start position
	exons_to_plot = thischrom[thischrom['exon_start']<end]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_end']>=end]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_start']>=start]
	zipped = zip(exons_to_plot['exon_start'].tolist(),exons_to_plot['exon_end'].tolist())
	[all_exons_to_plot.add(z) for z in zipped]

	# situation 4: clique is entirely contained within exon
	exons_to_plot = thischrom[thischrom['exon_start']<start]
	exons_to_plot = exons_to_plot[exons_to_plot['exon_end']>end]
	zipped = zip(exons_to_plot['exon_start'].tolist(),exons_to_plot['exon_end'].tolist())
	[all_exons_to_plot.add(z) for z in zipped]

	# plot all exons
	all_exons_to_plot = sorted(all_exons_to_plot, key=lambda x:x[0])
	for i,(s,e) in enumerate(all_exons_to_plot):
		ax.plot([s,e], [-(i+1), -(i+1)], c='b', linewidth=3)


if __name__=="__main__":
	EXONS = pd.read_csv(CONSTANTS['FILE_EXONS'])
	clique = sys.argv[2]
	print "Plotting clique {}".format(clique)

	df = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', header=None, names=CONSTANTS['FILE_ALIGNMENTS_COLS'])

	# add cliqueid
	df = df.merge(pd.read_csv(CONSTANTS['FILE_READS_TO_CLIQUES'], sep='\t'), on='read_id')
	df = df[df['cliqueid']==clique]

	# add clusterid
	df_tmp = pd.read_csv(CONSTANTS['FILE_READS_TO_CLUSTERS'], sep='\t')
	df = df.merge(df_tmp, on='read_id')

	# get begin/end of clique
	df_clique = pd.read_csv(CONSTANTS['FILE_CLIQUES'], sep='\t')
	df_clique = df_clique[df_clique['cliqueid']==clique]
	clique_start, clique_end = [df_clique['clique_start'].iloc[0], df_clique['clique_end'].iloc[0]]

	# get mi
	df_clique = pd.read_csv(CONSTANTS['FILE_CLIQUE_TABLE'], sep='\t')
	df_clique = df_clique[df_clique['cliqueid']==clique]
	mi = df_clique['MI'].iloc[0]

	with PdfPages('{}/clique_plots_{}'.format(CONSTANTS['DIR_CLIQUE_PLOTS'], clique)) as pdf:
		gene_rmct(pdf, df, mi, clique)
		plot_clique(pdf, df, mi, clique, clique_start, clique_end)

	



