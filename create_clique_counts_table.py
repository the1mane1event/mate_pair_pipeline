import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def summarize_by_clique_and_cluster(df):
	grouped = df.groupby(['geneid', 'cliqueid', 'Cluster_Start', 'Cluster_End'])
	counts = pd.DataFrame(grouped.size().rename('count'))
	counts = counts.reset_index()

	counts.to_csv(CONSTANTS['FILE_CLIQUE_COUNTS_TABLE'], index=False, sep='\t')


def main():
	df = pd.read_csv(CONSTANTS['FILE_READS_TO_CLUSTERS'], sep='\t')
	#print df.shape, df.columns

	df2 = pd.read_csv(CONSTANTS['FILE_CLIQUES_TO_GENES'], sep='\t')
	#print df2.shape, df2.columns


	df = df.merge(df2, on='cliqueid')
	# print df.shape, df.columns
	# print df.iloc[:3,:]

	summarize_by_clique_and_cluster(df)

if __name__=='__main__':
	main()

