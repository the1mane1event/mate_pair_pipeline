import sys, os, math, time, shutil
import numpy as np
import matplotlib.pyplot as plt
import pysam, io
save_stdout = sys.stdout
sys.stdout = io.BytesIO()

from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

def write_transcript(fn, line):
	named_fields = line[8].split(';')

	gene_id = named_fields[0].strip().split(' ')[1]
	transcript_id = named_fields[1].strip().split(' ')[1]

	start = line[3]
	end = line[4]
	strand = line[6]
	chrom = line[0]

	gene_id = gene_id.replace('"','').replace(';','')
	transcript_id = transcript_id.replace('"','').replace(';','')
	strand = '1' if strand=='+' else '-1'
	chrom = chrom.replace('chr','')

	out = '{}\n'.format('\t'.join([gene_id,transcript_id,start,end,strand,chrom]))
	fn.write(out)

def write_exon(fn, line):
	chrom = line[0]
	exon_start = line[3]
	exon_end = line[4]
	out = "{}\n".format(','.join([chrom,exon_start,exon_end]))

	fn.write(out)

def write():
	with open(CONSTANTS['FILE_GTF']) as f1:
		with open(CONSTANTS['FILE_EXONS'], 'w+') as f2:
			with open(CONSTANTS['FILE_TRANSCRIPTTABLE'], 'w+') as f3:
				counter=0
				f2.write('chrom,exon_start,exon_end\n')
				f3.write('gene_id\ttranscript_id\tstart\tend\tstrand\tchrom\n')
				for line in f1:
					counter+=1
					line = line.split('\t')
					if len(line)<=2: continue

					if line[2]=='transcript':
						write_transcript(f3, line)
						
					elif line[2]=='exon':
						write_exon(f2, line)					
write()
