#!/bin/bash
# inputs: 
#    genome sequence FASTA files (comma separated)
#    genome annotation GTF file
# outputs:
#    aligner reference
#    transcript table
set -e;
echo "`date +'%m/%d %H:%M:%S'` Starting 1_reference_compiler";
# rsem-prepare-reference \
#     --star \
#     --star-sjdboverhang=250 \
#     --gtf $2 \
#     $3 \
#     "$1"/ref/ref;

space_separated="${3//,/ }";
export starcommand='
STAR
 --runThreadN 1
 --runMode genomeGenerate
 --genomeDir '$1'/ref
 --genomeFastaFiles '$space_separated'
 --sjdbGTFfile '$2'
 --sjdbOverhang 250
 --outFileNamePrefix '$1'/ref/ref
'
echo $starcommand;
$starcommand;

python "$BASEDIR"/create_transcript_table.py $results_dir;
echo "`date +'%m/%d %H:%M:%S'` Done";