import pandas as pd
import numpy as np
import warnings, time, sys, random, shutil, os, io
warnings.filterwarnings("ignore", category=DeprecationWarning)
random.seed(1)
save_stdout = sys.stdout
sys.stdout = io.BytesIO()
from create_constants import load_variables
CONSTANTS = load_variables(sys.argv[1])

from scipy.stats import chi2_contingency
group_col = 'cliqueid'

def calc_mi(df):
	counts = pd.crosstab(df.iloc[:,-2], df.iloc[:,-1]).as_matrix()

	MI = -1.
	if len(counts.shape)>=2 and counts.shape[0]>1 and counts.shape[1]>1:
		# MI = metrics.mutual_info_score(df.iloc[:,-2], df.iloc[:,-1])
		n = counts.sum()
		row_marginal = counts.sum(axis=1)
		col_marginal = counts.sum(axis=0)

		cell_joint = 1.* counts / n
		row_marginal = 1.* row_marginal / n
		col_marginal = 1.* col_marginal / n

		MI = 0.
		for i in xrange(cell_joint.shape[0]):
			for j in xrange(cell_joint.shape[1]):
				p_x = row_marginal[i]
				p_y = col_marginal[j]
				p_xy = cell_joint[i,j]

				if p_xy != 0:
					val = p_xy * np.log2(1.* p_xy / (p_x*p_y))
					MI += val

	return MI

def calc_pvalue(df):
	counts = pd.crosstab(df.iloc[:,-2], df.iloc[:,-1]).as_matrix()

	p_value = -1.
	if len(counts.shape)>=2 and counts.shape[0]>1 and counts.shape[1]>1:
		test_stat, p_value, deg_freedom, expected_counts = chi2_contingency(counts)

	return p_value

def num_clusters(df):
	counts = pd.crosstab(df.iloc[:,-2], df.iloc[:,-1]).as_matrix()

	return counts.shape[0], counts.shape[1]

def expression_vs_mi(df):
	df_tmp = df.iloc[:,:]
	grouped = df_tmp[[group_col, CONSTANTS['START_CLUSTER_COL'], CONSTANTS['END_CLUSTER_COL']]].groupby(group_col)
	####################################################
	# get MI
	t = time.time()
	print "About to calc MI..."
	agged_mi = grouped.apply(calc_mi)
	combined = pd.concat([agged_mi], axis=1)
	combined.columns = ['MI']
	combined[group_col] = combined.index
	print "Done. MI took {:.2f} s".format(time.time() - t)
	agged = combined
	####################################################


	####################################################
	# get p-value
	t = time.time()

	print "About to calc p-value..."
	agged_pvalue = grouped.apply(calc_pvalue)
	agged_pvalue.name = 'p-value'
	print "Done. p-value took {:.2f} s".format(time.time() - t)

	
	agged_pvalue = pd.DataFrame({group_col:agged_pvalue.index, 'p-value':agged_pvalue.values})
	agged_pvalue = agged_pvalue.merge(agged, left_on=group_col, right_on=group_col)
	agged = agged_pvalue
	####################################################

	#del agged['Gene_Name_Start']
	agged = agged.loc[np.logical_and(agged['p-value']!=-1, agged['MI']!=-1)]

	return agged

def benjamini_hochberg(pvalues, correction_type = "Benjamini-Hochberg"):
	"""                                                                                                   
	consistent with R - print correct_pvalues_for_multiple_testing([0.0, 0.01, 0.029, 0.03, 0.031, 0.05, 0.069, 0.07, 0.071, 0.09, 0.1]) 
	"""
	from numpy import array, empty                                                                        
	pvalues = array(pvalues) 
	n = float(pvalues.shape[0])                                                                           
	new_pvalues = empty(n)                                                         
	if correction_type == "Benjamini-Hochberg":                                                         
		values = [ (pvalue, i) for i, pvalue in enumerate(pvalues) ]                                      
		values.sort()
		values.reverse()                                                                                  
		new_values = []
		for i, vals in enumerate(values):                                                                 
			rank = n - i
			pvalue, index = vals                                                                          
			new_values.append((n/rank) * pvalue)                                                          
		for i in xrange(0, int(n)-1):  
			if new_values[i] < new_values[i+1]:                                                           
				new_values[i+1] = new_values[i]                                                           
		for i, vals in enumerate(values):
			pvalue, index = vals
			new_pvalues[index] = new_values[i]                                                                                                                  
	return new_pvalues

def main():
	df_orig = pd.read_csv(CONSTANTS['FILE_READS_TO_CLUSTERS'], sep='\t')
	
	# add mi, p-value, and adjusted p-value
	df = expression_vs_mi(df_orig)

	df['p-value_adjusted'] = benjamini_hochberg(df['p-value'].tolist())

	# add counts
	counts = df_orig['cliqueid'].value_counts().reset_index()
	counts.columns = ['cliqueid', 'count']
	df = df.merge(counts, on='cliqueid')

	# add strand
	df['strand'] = df['cliqueid'].map(lambda x: x.split('_')[1])

	# add clique interval coordinates
	df_clique = pd.read_csv(CONSTANTS['FILE_CLIQUES'], sep='\t')
	df = df.merge(df_clique, on='cliqueid')

	# add geneid
	df_genes = pd.read_csv(CONSTANTS['FILE_CLIQUES_TO_GENES'], sep='\t')
	df = df.merge(df_genes, on='cliqueid')

	# add min/max read per clique
	df_reads_to_cliques = pd.read_csv(CONSTANTS['FILE_READS_TO_CLIQUES'], sep='\t')
	df_reads = pd.read_csv(CONSTANTS['FILE_ALIGNMENTS_FILTERED'], sep='\t', header=None, names=CONSTANTS['FILE_ALIGNMENTS_COLS'])
	df_reads = df_reads.merge(df_reads_to_cliques, on='read_id')
	df_reads = df_reads[['cliqueid', 'read1_5p', 'read2_3p']]
	rows = pd.DataFrame()
	for strand in [1,-1]:
		for chrom in CONSTANTS['CHROMS']:
			chromstrand = '{}_{}'.format(chrom[3:],strand)
			
			df_reads_tmp = df_reads[df_reads['cliqueid'].map(lambda x: '_'.join(x.split('_')[:2]))==chromstrand]
			df_tmp = df[df['cliqueid'].map(lambda x: '_'.join(x.split('_')[:2]))==chromstrand]
			print chromstrand,  df_reads_tmp.shape, df_tmp.shape
			df_tmp_merged = df_tmp.merge(df_reads_tmp, on='cliqueid')

			if strand==1:
				min_coords = df_tmp_merged.groupby(['cliqueid']).apply(lambda x: x['read1_5p'].min())
				max_coords = df_tmp_merged.groupby(['cliqueid']).apply(lambda x: x['read2_3p'].max())
			else:
				min_coords = df_tmp_merged.groupby(['cliqueid']).apply(lambda x: x['read2_3p'].min())
				max_coords = df_tmp_merged.groupby(['cliqueid']).apply(lambda x: x['read1_5p'].max())
			total = pd.concat([min_coords, max_coords], axis=1)
			rows = pd.concat([rows, total], axis=0)


	rows = rows.reset_index()
	rows.columns = ['cliqueid', 'min_coordinate', 'max_coordinate']

	df = df.merge(rows, on='cliqueid')

	# reorder columns
	df = df[['geneid', 'cliqueid', 'clique_start', 'clique_end', 'min_coordinate', 'max_coordinate', 'strand', 'count', 'MI', 'p-value', 'p-value_adjusted']]

	# write out
	df.to_csv(CONSTANTS['FILE_CLIQUE_TABLE'], sep='\t', index=False)
	print "Done!"


if __name__=='__main__':
	main()



	




